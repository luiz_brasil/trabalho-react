import styled from 'styled-components/native';
import { style } from '../../global/global';

export const Container = styled.View`
height:812px;
width:375px;
min-height:812px;
min-width:375px;
`
//Estilização do background
export const Background = styled.Image`
height:100%;
width:100%;
position:absolute;
`
//Estilização da logo
export const Logo = styled.Image`
width:53.33%;
height:24.63%;
position:relative;
align-self:center;
margin: 9.87% 0 21.33% 0;
`
//Estilização da Box que contem os campos de texto para login
export const CampoLogin = styled.View`
height:148px;
width:327px;    
align-self:center;
margin-bottom: 3px;
flex-direction:columns;
justify-content:space-between;
`
//Estilização dos campos de texto para login
export const CampoTexto = styled.View`
height:52px;
width:328px;
border: 1px solid #E3E3EA;
border-radius: 10px;
flex-direction:row;
align-items:center;
`
//Estilização do icone de User
export const Usuario = styled.Image`
height:35px;
width:35px;
margin-left:8px;
margin-right:6.87px;
`
//Estilização do icone de Senha
export const Senha = styled.Image`
height:35px;
width:19px;
margin-left:18.22px;
margin-right:13.57px;
`
//Estilização do icone de Ocultar senha
export const Olho = styled.Image`
height:22.5px;
width:20.5px;
margin-right:17.16px;
`
//Estilização da Box que contem o texto Esqueci minha senha
export const CampoTextoEsqueceu = styled.View`
height:18px;
width:124px;
margin-bottom:51px;
margin-left:227px;
`
//Estilização do texto Esqueci minha senha
export const EsqueceuSenha = styled.Text`
align-self:right;
font-style: normal;
font-weight: 400;
font-size: 12px;
color:${style.colors.mainWhite};
`
//Estilização do placeholder do campo de texto para login
export const Texto = styled.Text`
width: 248.37px;
height: 16px;
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 16px;
color:${style.colors.mainWhite};
`
//Estilização do Botão Entrar
export const CatchBtn = styled.TouchableOpacity`
height:52px;
width:327px;
background-color:${style.colors.weakWhite};
border: 3px solid ${style.colors.mainWhite};
border-radius:10px;
padding-top:8px;
margin-bottom:25px;
align-items:center;
align-self:center;
`
//Estilização do texto interior ao botão
export const BtnTxt = styled.Text`
font-weight: 700;
font-size: 20px;
color:${style.colors.mainWhite};
`
//Estilização da Box que contem os logins sociais
export const CampoSocial = styled.View`
height:85px;
width:327px;
align-self:center;
flex-direction:column;
justify-content:space-between;
margin-bottom:45px;
`
//Estilização da Box que contem o texto Entre Com
export const CampoEntreCom = styled.View`
height:20px;
width:325px;
flex-direction:row;
align-self:center;
align-items:center;
justify-content:center;
`
//Estilização das linhas
export const Linha = styled.View`
height:1px;
width:115;
background-color:${style.colors.mainWhite};
border: 1px solid ${style.colors.mainWhite};
border-radius: 10px;
`
//Estilização do texto Entre com
export const TextoEntreCom = styled.View`
font-style: normal;
font-weight: 700;
font-size: 15px;
line-height: 20px;
margin: 0 8px 0 8px;
color: ${style.colors.mainWhite};
`
//Estilização da Box que contem os botões de login social
export const BtnsSociais = styled.View`
height:52px;
width:302px;
align-self:center;
flex-direction:row;
justify-content:space-between;
`
//Estilização dos botões de login social
export const BtnSocial = styled.TouchableOpacity`
height:52px;
width:122px;
border: 1px solid ${style.colors.mainWhite};
border-radius: 10px;
align-items:center;
justify-content:center;
`
//Estilização do icone do google
export const LogoGoogle = styled.Image`
height:21px;
width:20px;
`
//Estilização do icone do facebook
export const LogoFacebook = styled.Image`
height:21px;
width:21px;
`
//Estilização da Box que contem o texto Crie uma conta
export const CampoCrieConta = styled.View`
height:18px;
width:233px;
align-self:center;
`
//Estilização do texto Crie uma conta
export const TextoCrieConta = styled.Text`
font-style: normal;
font-weight: 400;
font-size: 14px;
line-height: 18px;
color:${style.colors.mainWhite};
`