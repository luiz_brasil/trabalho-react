import React from "react";
import { Background, CampoLogin, CatchBtn, BtnTxt, Container, Logo, LogoGoogle, LogoFacebook, CampoTexto, Texto, Usuario, Senha, Olho, EsqueceuSenha, CampoTextoEsqueceu, CampoSocial, BtnsSociais, BtnSocial, CampoCrieConta, TextoCrieConta, CampoEntreCom, TextoEntreCom, Linha} from "./style";

const Login = ()=> {
    return (
        <Container>
        <Background source={require('../../assets/background.png')}/>
        <Logo source={require('../../assets/logo_reus-e.png')}/>
        <CampoLogin>
        <CampoTexto>
            <Usuario source={require('../../assets/user.png')}/>
            <Texto>Digite seu usuário</Texto>
        </CampoTexto>
        <CampoTexto>
            <Senha source={require('../../assets/chave.png')}/>
            <Texto>Digite sua senha</Texto>
            <Olho source={require('../../assets/olho.png')}/>
        </CampoTexto>
        </CampoLogin>
        <CampoTextoEsqueceu>
            <EsqueceuSenha>Esqueci minha senha</EsqueceuSenha>
        </CampoTextoEsqueceu>        
        <CatchBtn>
            <BtnTxt>Entrar</BtnTxt>
        </CatchBtn>
        <CampoSocial>
            <CampoEntreCom>
                <Linha></Linha>
                <TextoEntreCom>
                    Ou entre com
                </TextoEntreCom>
                <Linha></Linha>
            </CampoEntreCom>
            <BtnsSociais>
                <BtnSocial>
                    <LogoGoogle source={require('../../assets/google.png')}/>
                </BtnSocial>
                <BtnSocial>
                <LogoFacebook source={require('../../assets/facebook.png')}/>
                </BtnSocial>
            </BtnsSociais>
        </CampoSocial>
        <CampoCrieConta>
            <TextoCrieConta>
                Não é registrado? Crie uma conta.
            </TextoCrieConta>
        </CampoCrieConta>
        </Container>
    );
}

export default Login;