import React from "react";
import { Background, CampoCadastro, CatchBtn, BtnTxt, Container, Logo, CampoTexto, Texto, Usuario, Email, Senha, Olho } from "./style";

const Cadastro = ()=> {
    return (
        <Container>
        <Background source={require('../../assets/background.png')}/>
        <Logo source={require('../../assets/logo_reus-e.png')}/>
        <CampoCadastro>
        <CampoTexto>
            <Usuario source={require('../../assets/user.png')}/>
            <Texto>Digite seu usuário</Texto>
        </CampoTexto>
        <CampoTexto>
            <Email source={require('../../assets/arroba.png')}/>
            <Texto>Digite seu email</Texto>
        </CampoTexto>
        <CampoTexto>
            <Senha source={require('../../assets/chave.png')}/>
            <Texto>Digite sua senha</Texto>
            <Olho source={require('../../assets/olho.png')}/>
        </CampoTexto>
        <CampoTexto>
            <Senha source={require('../../assets/chave.png')}/>
            <Texto>Confirme sua senha</Texto>
            <Olho source={require('../../assets/olho.png')}/>
        </CampoTexto>
        </CampoCadastro>
        <CatchBtn>
            <BtnTxt>Cadastrar</BtnTxt>
        </CatchBtn>
        </Container>
    );
}

export default Cadastro;