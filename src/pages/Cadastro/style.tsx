import styled from 'styled-components/native';
import { style } from '../../global/global';

export const Container = styled.View`
height:812px;
width:375px;
`
//Estilização do background
export const Background = styled.Image`
height:100%;
width:100%;
position:absolute;
`
//Estilização da logo
export const Logo = styled.Image`
width:46.67%;
height:21.55%;
position:relative;
align-self:center;
margin: 7.47% 26.67% 12.53% 26.67%;
`
//Estilização da Box que contem os campos de texto para cadatro
export const CampoCadastro = styled.View`
height:41.87%;
width:87.47%;    
align-self:center;
margin-bottom: 15.2%;
flex-direction:columns;
justify-content:space-between;
`
//Estilização dos campos de texto para cadastro
export const CampoTexto = styled.View`
height:52px;
width:328px;
border: 1px solid #E3E3EA;
border-radius: 10px;
flex-direction:row;
align-items:center;
`
//Estilização do icone de User
export const Usuario = styled.Image`
height:35px;
width:35px;
margin-left:8px;
margin-right:6.87px;
`
//Estilização do icone de Email
export const Email = styled.Image`
height:35px;
width:38.5px;
margin-left:8px;
margin-right:6.87px;
`
//Estilização do icone de Senha
export const Senha = styled.Image`
height:35px;
width:19px;
margin-left:18.22px;
margin-right:13.57px;
`
//Estilização do icone de Ocultar senha
export const Olho = styled.Image`
height:22.5px;
width:20.5px;
margin-right:17.16px;
`
//Estilização do placeholder do campo de texto para cadastro
export const Texto = styled.Text`
width: 248.37px;
height: 16px;
font-style: normal;
font-weight: 400;
font-size: 16px;
line-height: 16px;
color:${style.colors.mainWhite};
`
//Estilização do Botão Cadastrar
export const CatchBtn = styled.TouchableOpacity`
height:52px;
width:327px;
background-color:${style.colors.weakWhite};
border: 3px solid ${style.colors.mainWhite};
border-radius:10px;
padding-top:8px;
align-items:center;
align-self:center;
`
//Estilização do texto interior ao botão
export const BtnTxt = styled.Text`
font-weight: 700;
font-size: 20px;
color:${style.colors.mainWhite};
`