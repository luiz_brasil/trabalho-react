import Login from './src/pages/Login';
import Cadastro from './src/pages/Cadastro';
import { useFonts, Roboto_300Light, Roboto_400Regular, Roboto_500Medium, Roboto_700Bold, Roboto_900Black } from '@expo-google-fonts/roboto'
import React from 'react';
import AppLoading from 'expo-app-loading'

export default function App() {

  let [fontsLoaded, error] = useFonts({ Roboto_300Light, Roboto_400Regular, Roboto_500Medium, Roboto_700Bold, Roboto_900Black});
  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <>
    <Cadastro/>
    </>
  );
}